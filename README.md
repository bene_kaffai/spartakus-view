# README Spartakus View #


### SYSTEM ###

Auf Basis eines Protokolls auf MQTT Basis sprechen diverse Entitäten in einem Netzwerk miteinander. Entitäten können hier sowohl Hardwareobjekte sein (WEMOS/ARDUINO Microcontroller mit angeschlossenen Sensoren/Aktoren), fertige IoT Lösungen (Philips HUE, etc.), oder Softwareanwendung (Twitter, Python Skripte, Processing Sketche, Telegram Messenger, etc.). Diese Entitäten sind als Knotenpunkte (Nodes) gedacht. Ihre In- und Outputs können miteinander verknüpft werden.


### VIEW ###

Die View ist die grafische Repräsentationen der Logik mit ihren Datenbanken über vorhanden EoI und die Regeln der Verknüpfung. Sie funktioniert im Prototyp als GUI. Es zeigt die EoIs als Knotenpunkte mit ihren Ein- und Ausgängen. Au-
ßerdem werden, im Sinne der visuellen Programmierung, die Regeln als Verbindungslinie zwischen Nodes angezeigt.
Sie ist interaktiv und kann von der Nutzer_in zur Erstellung von Regeln und zur Erzeugung von Logikbausteinen genutzt werden. Die View kommuniziert laufend mit der Logik. Sobald in der Logik oder der View ein Ereignis eintritt wird der je-
weils andere Teil benachrichtigt.

* Basierend auf JointJS v1.0.1 (2016-09-20) - terms of the Mozilla Public
License, v. 2.0. http://mozilla.org/MPL/2.0/
* Version 0.1
* GUI für das Spartakus Framework als Beispiel für die CeBit 2017

### How do I get set up? ###

* Python 3 installieren
* Python Dependencies installieren: paho.mqtt.client, rethinkdb, json, time ``` pip3 install paho-mqtt rethinkdb```
* einen beliebigen MQTT-Broker im Netzwerk starten ``` Defaults: MQTTP_HOST = '192.168.0.100' MQTTP_PORT = 1883```
* RethinkDB installieren und starten ``` Defaults DB_HOST = 'localhost' DB_PORT = 28015```
* ``` python3 logic.py ``` aus  [Logic](https://bitbucket.org/bene_kaffai/spartakus-logic/overview) starten
* ``` index.html ``` im Browser öffnen


### Weitere Tools für das Spartakus Framework ###

* [Logic](https://bitbucket.org/bene_kaffai/spartakus-logic/overview) (Kommunikationszentrale des Systems)
* [Logic Tings](https://bitbucket.org/bene_kaffai/spartakus-logic-thing) (einfache Logikbausteine AND, OR, COMPARE, etc. um Entitäten komplex miteinander zu verknüpfen)
* [WEMOS](https://bitbucket.org/bene_kaffai/spartakus-wemos-template) und [PYTHON](https://bitbucket.org/bene_kaffai/spartakus-python-template) Templates (um eigene Entitäten im Netzwerk verfügbar zu machen)

### Beispiel ###


### Who do I talk to? ###

* Repo owner or admin: bene_kaffai, https://bitbucket.org/MaxDemian/, https://bitbucket.org/BenHatscher/
