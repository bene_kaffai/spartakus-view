//sample HTML/JS script that will publish/subscribe to topics in the Google Chrome Console
//by Matthew Bordignon @bordignon on twitter.

// local mosquitto broker muss listener für websocket öffnen
// sudo nano /etc/mosquitto/conf.d/websocket.conf
// "listener 1883
// listener 1884
// protocol websockets"

var VIEW = {
    id:"VIEW"
}
var Spartakus = {
    host: '192.168.0.100', // 192.168.0.100 "broker.hivemq.com",
    port:1884, // 8000
    network: '/spartakus/thing',
    change: {
        thingsadd: '/spartakus/db/LOGIC/output/change/things/add',
        thingsremove: '/spartakus/db/LOGIC/output/change/things/remove',
        rulesadd: '/spartakus/db/LOGIC/output/change/rules/add',
        rulesremove: '/spartakus/db/LOGIC/output/change/rules/remove',
        value: '/spartakus/db/LOGIC/output/change/value'
    }
}

var client = new Paho.MQTT.Client(Spartakus.host, Spartakus.port, VIEW.id);

client.onConnectionLost = function (responseObject) {
    console.log("connection lost: " + responseObject.errorMessage);
};

client.onMessageArrived = function (message) {
    switch(message.destinationName) {
        // füge neue node cell hinzu
        case Spartakus.change.thingsadd:
        createCell(JSON.parse(message.payloadString))
        console.log('mqtt in: add thing');
        break;
        // lösche eine thing, was auch aus der datenbank entfernt wurde
        case Spartakus.change.thingsremove:
        removeCell(JSON.parse(message.payloadString).id);
        console.log('mqtt in: remove thing');
        break;
        // erstelle einen neuen link zwischen dingen
        case Spartakus.change.rulesadd:
        var pl = JSON.parse(message.payloadString);
        createLink(graph.getCell(pl.sender.id),pl.sender.output, graph.getCell(pl.receiver.id),pl.receiver.input, pl.id);
        console.log('mqtt in:  added rule');
        break;
        // lösche einen link, sofern er vorhanden ist
        case Spartakus.change.rulesremove:
        var pl = JSON.parse(message.payloadString);
        // console.log(pl.id);
        removeLink(pl.id);
        console.log('mqtt in:  remove rule');
        break;
        case Spartakus.change.value:
        var pl = JSON.parse(message.payloadString);
        for (o in pl.output) {
            changeValue(pl.id, 'out:'+o, pl.output[o]);
        }
        console.log('changed outport value:');
        break;
        default:
        console.log(message.destinationName, ' -- ', message.payloadString);
    }
};

var options = {
    timeout: 3,
    onSuccess: function () {
        console.log("mqtt connected");
        // Connection succeeded; subscribe to our topic, you can add multile lines of these
        for (var key in Spartakus.change){
            //console.log(Spartakus.change[key]);
            client.subscribe(Spartakus.change[key], {qos: 1});
        }

        //use the below if you want to publish to a topic on connect
        message = new Paho.MQTT.Message("Hello World. The View is online!");
        message.destinationName = Spartakus.network+"/"+VIEW.id+"/output";
        client.send(message);

        message2 = new Paho.MQTT.Message('1');
        message2.destinationName = Spartakus.network+'/LOGIC/input/dump';
        client.send(message2);
    },
    onFailure: function (message) {
        console.log("Connection failed: " + message.errorMessage);
    }
};

function sendMsg_MQTT(msg,_topic){
    message = new Paho.MQTT.Message(msg);
    message.destinationName = Spartakus.network+"/"+VIEW.id+"/output/"+_topic;
    client.send(message);
}

function sendMenu_MQTT(_thing,_input){
    message = new Paho.MQTT.Message('1');
    message.destinationName = Spartakus.network+"/"+_thing+"/input/"+_input;
    client.send(message);
}

function sendMESSAGE_0_MQTT(_thing, _msg){
    message = new Paho.MQTT.Message(_msg);
    message.destinationName = Spartakus.network+"/"+_thing+"/input/string";
    client.send(message);
}

function sendTOGGLE_0_MQTT(_thing){
    message = new Paho.MQTT.Message('1');
    message.destinationName = Spartakus.network+"/"+_thing+"/input/trigger";
    client.send(message);
}

//  expecting '{"time":"Szene1", "sender":{"name":"ESP1_POTI", "output":"poti"}, "receiver":{"name":"ESP1_LED", "input":"pwm"}}'
function removeRule_MQTT(_id){
    var myObj = {};
    myObj["id"] = _id;
    var json = JSON.stringify(myObj);
    var message = new Paho.MQTT.Message(json)
    message.destinationName = Spartakus.network+"/LOGIC/input/rule/remove";
    client.send(message);
}

function addRule_MQTT(_pendingLink){
    var myObj = {
        time:'Szene1',
        id: _pendingLink.id,
        sender: {
            name:graph.getCell(_pendingLink.get('source').id).name,
            id:_pendingLink.get('source').id,
            output: _pendingLink.get('source').port.replace('out:','')
        },
        receiver: {
            name:graph.getCell(_pendingLink.get('target').id).name,
            id:_pendingLink.get('target').id,
            input:_pendingLink.get('target').port.replace('in:','')
        }
    };
    var json = JSON.stringify(myObj);
    var message = new Paho.MQTT.Message(json)
    message.destinationName = Spartakus.network+"/LOGIC/input/rule/add";
    client.send(message);
}

// ask LOGIC for DB dump. NOT IMPLEMENTED!
function getDump_MQTT(){
    message = new Paho.MQTT.Message('1');
    message.destinationName = Spartakus.network+"/LOGIC/input/dump/all";
    client.send(message);
}

function initMQTT() {
    client.connect(options);
}

// dummy for html button
function DUMMYremoveRule_MQTT(_id){
    message = new Paho.MQTT.Message('{"time":"Szene1", "sender":{"name":"ESP1_POTI", "output":"poti"}, "receiver":{"name":"ESP1_LED", "input":"pwm"}}');
    message.destinationName = Spartakus.network+"/LOGIC/input/rule/remove";
    client.send(message);
}

// expecting '{"time":"Szene1", "sender":{"name":"ESP1_POTI", "output":"poti"}, "receiver":{"name":"ESP1_LED", "input":"pwm"}}'
function DUMMYaddRule_MQTT(msg){
    message = new Paho.MQTT.Message('{"time":"Szene1", "sender":{"name":"ESP1_POTI", "output":"poti"}, "receiver":{"name":"ESP1_LED", "input":"pwm"}}');
    message.destinationName = Spartakus.network+"/LOGIC/input/rule/add";
    client.send(message);
}
