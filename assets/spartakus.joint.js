var graph = null;
var paper = null;
var pendingLink = null; // hack - keep reference to last created link untol mouse is leseased
var pendingRemoval = null; // hack - reference to last removed link to not recreate on pointerup

function initJoint(){
    //console.log(getDump_MQTT())
    graph = new joint.dia.Graph;

    paper = new joint.dia.Paper({
        el: document.getElementById('paper'),
        width: 1800,
        height: 800,
        gridSize: 1,
        model: graph,
        snapLinks: true,
        linkPinning: true,
        embeddingMode: true,
        highlighting: {
            'default': {
                name: 'stroke',
                options: {
                    padding: 6
                }
            },
            'embedding': {
                name: 'addClass',
                options: {
                    className: 'highlighted-parent'
                }
            }
        },
        validateEmbedding: function(childView, parentView) {

            return parentView.model instanceof joint.shapes.devs.Coupled;
        },

        validateConnection: function(sourceView, sourceMagnet, targetView, targetMagnet) {

            return sourceMagnet != targetMagnet;
        }
    });

    paper.on('cell:pointerup', function(cellView, evt, x, y) {
        if (pendingLink != null) {

            // hack to prevent link return - pointerdown remembers link and recreates link
            if (pendingRemoval != pendingLink.id) {
                addRule_MQTT(pendingLink);
            }
            // console.log(pendingLink);
            pendingLink.remove();
        }
    });

    graph.on('change:source change:target', function(_object) {
        _object.set('router', { name: 'manhattan'});
        _object.set('connector', { name: 'rounded', args: { radius: 15 }});
        //_object.addTo(graph).reparent();
        if (_object.get('source').id && _object.get('target').id) {
            //console.log('link added between '+link.get('source').id+' to '+link.get('target').id);
            var source = _object.get('source');
            pendingLink = _object;
            // bad thing: these ids are just the jointJS-internal ids
        } else {
            pendingLink = null;
            // bad thing: these ids are just the jointJS-internal ids
        }
    })

    graph.on('remove', function(_object) {
        //console.log('link removed between '+link.get('source').id +' and '+link.get('target').id);
        //var source = graph.getCell(link.get('source')).attributes.outPorts;
        //console.log(_object);
    })

    // create some dummy cells and connections
    // exampleLayout();
}

// global function to create new node
var createCell = function(_msg){
    var msg = _msg; // JSON.parse(_msg)
    var inPortCount =Object.keys(msg.input).length-1; // bc every node hast a "report"-node, which is not drawn (see "msgToArray")
    var outPortCount =Object.keys(msg.output).length-1; // same same
    var highPortCount = inPortCount >=  outPortCount ? inPortCount : outPortCount;
    //var inPorts = msgToArray(msg.input,'in'); // not used, right?
    var newCell = new joint.shapes.devs.Model({
        position: {
            x: Math.floor((Math.random() * 700) + 1),
            y: Math.floor((Math.random() * 200) + 1)
        },
        size: {
            width: 130,
            height: highPortCount*30
        },
        id: msg.id,
        outPorts: msgToArray(msg.output,'out'),
        inPorts: msgToArray(msg.input,'in')
    });
    newCell.name = msg.name // 'new Cell ' + Math.floor((Math.random() * 10) + 1);
    newCell.attr('text/text', newCell.name);

    if (msg.type == "logic") { // hide specific nodes
        newCell.attr('./display', 'none');
    }
    // make buttons out of menu nodes
    else if (msg.type == "menu") { // hide specific nodes
        newCell.attr('./display', 'none');
        for (var property in msg.input) {
            if (property == 'message') {
                addButtons(msg.name,property);
                addMsgField(msg.name,property)
            } else if(property == 'toggle'){
                addButtons(msg.name,property);
                addToggleBtn(msg.name,property)
            }
            else if (property != 'report' && property != 'children' && property != 'remove') {
                addButtons(msg.name,property);
            }
        }
    }
    else if (msg.type == "hardware") { // hide specific nodes
        // color change does not work
    }

    newCell.attr({
        '.body': {
            'rx': 5,
            'ry': 5
        }
    });
    graph.addCells([newCell]);
    // resize with by cell name works only after adding element to paper
    var cellView = paper.findViewByModel(newCell); // get cell view
    text = cellView.$("text"); //get shape element
    bbox = text[0].getClientRects()[0]; // idk
    newCell.resize(bbox.width+40, newCell.attributes.size.height); //resize with new with but old height
}

// global function when value has changed
var changeValue = function(_id, _output, _value) {
  var _cell = graph.getCell(_id);
  var _port = _cell.getPort(_output);

  // ugly hack - _port is no real reference to the port whre you can change anything -.-, but _cell is
  for (var i = 0; i < _cell.portData.ports.length; i++) {
    if (_cell.portData.ports[i].id == _port.id) {
        // TODO: tet is changed in .attrs['.port-label'], but it's not displayed in browser (ubuntu chrome+firefox)
      _cell.portData.ports[i].attrs['.port-label'].text = _output + " "+_value;
    }
  }
}

function addMsgField(parent, type){
    // sendMenu_MQTT(parent,type);
    //Create an input type dynamically.
    var element = document.createElement("input");
    //Assign different attributes to the element.
    element.type = 'text';
    element.value = 'MESSAGE_0';
    element.name = 'MESSAGE_0'; // And the name too?
    element.onkeydown = function() { // Note this is a function
        // alert(element.name);
        // sendMenu_MQTT(parent,type);
        if(event.keyCode == 13) {
            sendMESSAGE_0_MQTT('MESSAGE_0',element.value);
        }
    };

    var foo = document.getElementById("tempInput");
    //Append the element in page (in span).
    foo.appendChild(element);
}

function addToggleBtn(parent, type){
    // sendMenu_MQTT(parent,type);
    //Create an input type dynamically.
    var element = document.createElement("input");
    //Assign different attributes to the element.
    element.type = 'button';
    element.value = 'TOGGLE_0'; // Really? You want the default value to be the type string?
    element.name = 'TOGGLE_0'; // And the name too?
    element.onclick = function() { // Note this is a function
        // alert(element.name);
        sendTOGGLE_0_MQTT('TOGGLE_0');
    };

    var foo = document.getElementById("tempInput");
    //Append the element in page (in span).
    foo.appendChild(element);
}

function addButtons(parent, type) {
    //Create an input type dynamically.
    var element = document.createElement("input");
    //Assign different attributes to the element.
    element.type = 'button';
    element.value = type; // Really? You want the default value to be the type string?
    element.name = parent; // And the name too?
    element.onclick = function() { // Note this is a function
        // alert(element.name);
        sendMenu_MQTT(parent,type);
    };

    var foo = document.getElementById("fooBar");
    //Append the element in page (in span).
    foo.appendChild(element);
}

var msgToArray = function(msg, type){
    var pins = []
    for (var property in msg) {
        if (msg.hasOwnProperty(property) && property.toString() != "report") {
            pins.push(strangePortFix(property.toString(),type))
        }
    }
    return pins
}

// BUG: if Ports have the same name they are not shown
// TEMP BUG FIX: add 'in' or 'out' to name
var strangePortFix = function(s,type){
    return type == 'in' ? 'in:'+s : 'out:'+s
}

// global function to create new node aka cells
var removeCell = function(_id){
    var cell = graph.getCell(_id);
    graph.removeCells([cell]);
    console.log('removed new Node ' + _id);
}

// connect two cell pins
var createLink = function(source, sourcePort, target, targetPort, _id) {
    // temporary fix to manage ports not showing BUG
    sourcePort =  strangePortFix(sourcePort, 'out') // 'out:'+sourcePort; //
    targetPort =  strangePortFix(targetPort, 'in') // 'in:'+targetPort; //

    var link = new joint.shapes.devs.Link({
        source: {
            id: source.id,
            port: sourcePort
        },
        target: {
            id: target.id,
            port: targetPort
        },
        id: _id
    });

    link.set('router', { name: 'manhattan'});
    link.set('connector', { name: 'rounded', args: { radius: 15 }});
    link.addTo(graph).reparent();
    link.on('change', function() { console.log("callback: link wurde verändert") });
    // link-deletion request
    link.on('remove', function(_object) {
        //[BE] BUG/ugly intended behaviour without warning: can't create a link with the same ID, need a new one for the recreated Link
        //[BE] workaround: add string to old ID
        var idParts = link.id.split(".");
        if (idParts[idParts.length-1] != 'remove') {
            // send delete request to logic
            removeRule_MQTT(link.id);
            pendingRemoval = link.id;
            //var removePendingLink = link;
            var removeid = link.id+'.remove'
            // create temporary link until mqtt remove order
            createLink(link.get('source').id,link.get('source').port,link.get('target').id,link.get('target').port, removeid);

        } else {
            console.log('removed temporary removelink');
        }

    })
    // remove temporary pending link, because it is created now
    pendingLink = null;
};

// remove link between to cells - to be called only by MQTT-message
var removeLink = function(_id){
    var id = _id+'.remove';
    var link = graph.getCell(id);
    // console.log(link)
    link.remove();
}

var exampleLayout = function(){
    var c1 = new joint.shapes.devs.Model({
        position: {
            x: 230,
            y: 50
        },
        size: {
            width: 130,
            height: 120
        }
    });
    c1.name = "testnode_30271dsfsdfsdf"
    c1.attr('text/text', c1.name);
    c1.set('inPorts', ['in:in']);
    c1.set('outPorts', ['out:out 1', 'out:out 2', 'out:out 3']);
    console.log(c1.id);

    var a1 = new joint.shapes.devs.Model({
        position: {
            x: 450,
            y: 260
        },
        size: {
            width: 130,
            height: 80
        },
        inPorts: ['in:xy'],
        outPorts: ['out:x', 'out:y']
    });

    var a2 = new joint.shapes.devs.Model({
        position: {
            x: 30,
            y: 160
        },
        size: {
            width: 130,
            height: 80
        },
        outPorts: ['out:out']
    });

    var a3 = new joint.shapes.devs.Model({
        position: {
            x: 650,
            y: 50
        },
        size: {
            width: 130,
            height: 80
        },
        inPorts: ['in:a', 'in:b']
    });

    graph.addCells([c1, a1, a2, a3]);

    changeValue(c1.id,'out:out 1','dummyValue');

    var label = c1.attr('.label/size');
    var cellView = paper.findViewByModel(c1);
    text = cellView.$("text"); //get shape element
    bbox = text[0].getClientRects()[0];
    c1.resize(bbox.width+40, c1.attributes.size.height);

    c1.embed(a1);

    createLink(a2, 'out', c1, 'in');
    createLink(c1, 'out 2', a1, 'xy');
    createLink(a1, 'x', a3, 'a');
    //createLink(a1, 'y', a3, 'b');
    createLink(c1, 'out 1', a3, 'a');
    //createLink(c1, 'out 2', a3, 'b');

    /* rounded corners */

    _.each([c1, a1, a2, a3], function(element) {

        element.attr({
            '.body': {
                'rx': 6,
                'ry': 6
            }
        });
    });
}
